/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                  This file is part of the program and library             */
/*         SCIP --- Solving Constraint Integer Programs                      */
/*                                                                           */
/*    Copyright (C) 2002-2020 Konrad-Zuse-Zentrum                            */
/*                            fuer Informationstechnik Berlin                */
/*                                                                           */
/*  SCIP is distributed under the terms of the ZIB Academic License.         */
/*                                                                           */
/*  You should have received a copy of the ZIB Academic License              */
/*  along with SCIP; see the file COPYING. If not visit scipopt.org.         */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   relax_lagr.c
 * @ingroup OTHER_CFILES
 * @brief  lagr relaxator
 * @author Dawit Hailu  
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/
//I'm gonna write this, just to check if it will upload right or not :) 
//what's up bro, this is just to check if i can pull it on git. 
//it worked buddy. now time to push it
#include <assert.h>
#include <string.h>
#include <chrono>
#include <iostream>
#include <math.h>


#include "relax_lagr.h"
#include "scip/scipdefplugins.h"
#include "scip/scip.h"
#include "scip/cons_countsols.h"
#include "scip/heur_undercover.h"

#include "probdata_lagr.h"
#include "vardata_lagr.h"




#define RELAX_NAME             "lagr"
#define RELAX_DESC             "relaxator"
#define RELAX_PRIORITY         536870911
#define RELAX_FREQ             0
#define RELAX_INCLUDESLP       FALSE   
#define RELAX_TIMING           SCIP_RELAXTIMING_BEFORENODE

/*
 * Data structures
 */

/* TODO: fill in the necessary relaxator data */

/** relaxator data */
struct SCIP_RelaxData

{
   SCIP_SOL* sol;         /**current solution(working solution)*/
   SCIP_VARDATA* vardata;
   SCIP_PROBDATA* probdata;
   int* bestsolvals;
   int* feasol;
   
   SCIP_Real lowerbound;

};

struct SCIP_VarData
{
   SCIP_VAR*                        var;
   int                              nVarConss;
   int                              nbadconssVarisin;                 /**<number of slot constraints the variable is occuring in*/  
   SCIP_Real                        varquotient;
   int*                             consscontainingvar;
   int*                             varids;
   int                              varindex;
   
};
 
/*create the variable data, specifically the column of the bad constraint. i.e. non-zero entries of the relaxed constraint matrix. It also saves the var index with the variable.*/
static
SCIP_RETCODE SCIPcreateVarData(
   SCIP* scip,
   SCIP_VAR** vars,
   SCIP_VAR** varbuffers, 
   int nSlotConss,
   int* badconss,
   int* consids,
   SCIP_VARDATA* vardata,
   FILE* file
)
{
   SCIP_Bool valid;
   for (int v = 0; v < SCIPgetNVars(scip); v++)
   { 
      int id = 0;
      int* consscontainingvar;
      int nbadconssVarisin=0;                         //will contains in how many of the relaxed constraints, a variable is found. This can be thought of as the sum of the column matrix. 
      int nconsvars = 0;
      SCIP_VAR* var = SCIPgetVars(scip)[v];           //get the variable

      int varindex = SCIPvarGetIndex(var)-SCIPvarGetIndex(vars[0]);             //get the index
      assert(varindex!= NULL);

      for (int r = 0; r < nSlotConss; ++r)            //nSlotConss is the number of rows of the relaxed constraint. it's found in the SCIPcreateprobdata function.
      {
         id = badconss[r];                            // this id is created in the case of presolving, just in case the constraints have been rearranged. We would like to save the ids of the relaxed conss in badconss
         SCIP_CONS* cons = SCIPgetConss(scip)[id];  //This gets the relaxed constraint. 
         // fprintf(file,"\n %s,%d \t",SCIPconsGetName(cons),r);
         SCIP_CALL(SCIPgetConsNVars(scip, cons, &nconsvars, &valid));    //get the number of variables in that constraint and save it to nconsvars
         SCIP_CALL(SCIPgetConsVars(scip, cons, varbuffers, nconsvars, &valid));   //get the variables themselves in the varbuffer array. 
         if (!valid){abort(); }           //If the nconsvars=0, or if Varbuffers has not gotten those variables, about this iteration. 

         for (int j = 0; j < nconsvars; ++j)             //create a loop of nconsvars to get all the variables in the constraint. 
         {
            SCIP_VAR* varx = varbuffers[j];              //remember that varbuffers contains all the variables in the constraint. 
            int varbufindex = SCIPvarGetIndex(varx)-SCIPvarGetIndex(vars[0]);     //get the index of the variable
            assert(varbufindex != NULL);
            // fprintf(file, "%s\t \t%d",SCIPvarGetName(varx),varbufindex);
            
            
            /**this is the main part. We check if vars[i] is in cons[id], and then save that constrain in consids. Remember that consids has been created outside this function with a size of the nslotconss. Meaning it is capable of saving all ids. */
            if (varindex == varbufindex)                                           /* (9) */
            {
               consids[nbadconssVarisin]=r;
               nbadconssVarisin++;
               // fprintf(file, " %s \t,",SCIPconsGetName(cons));
            }
         }
         
      }

      SCIP_CALL(SCIPallocBufferArray(scip, &consscontainingvar, nbadconssVarisin));    /*for a better allocation of memory, we only create enough array to save the non-zero elements of the column. the size is nbadconssVarisin.*/
      for(int t=0;t<nbadconssVarisin;t++)
      {
         consscontainingvar[t]=consids[t];
         // fprintf(file, "%d \t",consscontainingvar[t]);
      }


      // fprintf(file,"\n");

      SCIP_CALL(SCIPallocBlockMemory(scip , &vardata));     
      SCIP_CALL(SCIPduplicateBlockMemoryArray(scip, &(vardata->consscontainingvar), consscontainingvar, nbadconssVarisin));
      vardata->nbadconssVarisin = nbadconssVarisin;  /**saves the number of relaxeds constraints the variable was originally found it.*/
      vardata->consscontainingvar = consscontainingvar;  /**saves the relaxed constraints in which this variable is found in.*/
      vardata->varindex = varindex;                /**save the varindex, just for assurance, incase it changes. */
      /**set the variable data to the variable*/
      SCIPvarSetData(var,vardata);  
                  
   }
   return SCIP_OKAY;
}



/** destructor of relaxator to free user data (called when SCIP is exiting) */
static
SCIP_DECL_RELAXFREE(relaxFreelagr)
{  /*lint --e{715}*/


   SCIP_RELAXDATA* relaxdata;
   relaxdata = SCIPrelaxGetData(relax);
   SCIPfreeBlockMemory(scip, &relaxdata);
   SCIPrelaxSetData(relax,NULL);
   
   
   return SCIP_OKAY;
}


int SCIPvardataGetnbadconssVarisin(
	SCIP_VARDATA* vardata     /**< variable data */
)
 {
    return vardata->nbadconssVarisin;
 }

int* SCIPvardataGetconsscontainingvar(
	SCIP_VARDATA* vardata     /**< variable data */
)
 {
    return vardata->consscontainingvar;
 }

int SCIPvardatagetVarindex(
   SCIP_VARDATA* vardata
){
   return vardata->varindex;
}


static
SCIP_DECL_RELAXINIT(relaxInitlagr)
{  /*lint --e{715}*/
   
   
  return SCIP_OKAY;
}


static
SCIP_Real sciprelaxsolve(
   SCIP* scip, 
   SCIP** relaxscip,
   SCIP_VARDATA* vardata,
   SCIP_VAR** VARSinsidegoodconss,
   SCIP_Real* sumofduals,
   SCIP_Real** relaxsolval,
   SCIP_SOL** relaxsolution,
   SCIP_SOL*** allsolutions,
   int* nsols,
   int* feasol,
   int nSlotConss,                              //number of relaxed constraints
   int* increasingorder,
   int* listnconsvars,
   int* listconsvarids,
   int* arrangedgoodconss,
   int* inwhichgoodconss,
   SCIP_Real* weights,
   SCIP_Real* objval,
   FILE* iter,
   double* lowerbound,
   SCIP_Real** subgradients,
   SCIP_Real** dualmultipliers, 
   SCIP_Real upperbound,
   int k
)
{  
   SCIP_RELAXDATA* relaxdata;
   SCIP_Bool valid;
   SCIP_CONS** conss = SCIPgetConss(scip);
   SCIP_VAR** vars = SCIPgetVars(scip);
   SCIP_VAR** relaxvars = SCIPgetVars(*relaxscip);
   int nvars = SCIPgetNVars(scip);
   SCIP_SOL* getsol;
   SCIP_CALL( SCIPcreateSol(scip, &getsol, NULL) );
   /*change the weight of the variables according with the dual multipliers that it associates with(the rows it's found it) */
   for(int v=0;v<nvars;v++)
   {
      SCIP_VAR* var = vars[v];
      SCIP_VAR* relaxvar = relaxvars[v];
      
      double sum =0;
      (*relaxsolval)[v]=-1;

      vardata=SCIPvarGetData(var);
      int* varids = SCIPvardataGetconsscontainingvar(vardata);  
      assert(varids=!NULL);
      int nbadconssVarisin = SCIPvardataGetnbadconssVarisin(vardata);

      for(int t=0;t<nbadconssVarisin;t++)
      {
         sum -= (*dualmultipliers)[varids[t]];
         // fprintf(iter,"{%s, id= %d, dual=%f sum = %f\n",SCIPvarGetName(vars[v]),varids[t], (*dualmultipliers)[varids[t]],sum);
      }
      // if(sum+weights[v]>0){sum=SCIPinfinity(scip);} //make the weight of the variable zero if it's new weight is positive. 
      SCIP_CALL(SCIPaddVarObj((*relaxscip),relaxvar,sum));
   }
   SCIP_CALL(SCIPprintOrigProblem(*relaxscip,iter,"lp",FALSE));
   
   /*add of the offset value, which is the negative of the sum of the duals*/
   // SCIP_CALL(SCIPaddOrigObjoffset(*relaxscip,-1*(*sumofduals)));

   // SCIP_CALL( SCIPtransformProb(*relaxscip) );
   
   fprintf(iter, "\n sumofduals = %f \n",*sumofduals);

   /*solve the problem*/
   SCIP_CALL( SCIPsolve(*relaxscip) );
  
   /*subtract of the offset value, which is the negative of the sum of the duals*/
   // SCIP_CALL(SCIPaddOrigObjoffset(*relaxscip,-1*(*sumofduals)));

   /*get the number of solutions and the best solution*/   
   (*relaxsolution) = SCIPgetBestSol(*relaxscip) ;
   (*allsolutions) = SCIPgetSols(*relaxscip) ;
   (*nsols) = SCIPgetNSols(*relaxscip);
   fprintf(iter, "number of solutions %d \n",*nsols);
   // SCIP_CALL(SCIPprintSol((*relaxscip),(*relaxsolution),iter, FALSE ));
   SCIP_Real measurelowerbound=0;       //we get the lowerbound of differet solution (adjusted weights)^t*x - (*sumofduals)
   printf("\n msl=%f ",SCIPgetPrimalbound(*relaxscip));
   
   /*Generate feasible solutions from the solutions, and get the upper bound*/
   
   int checker = 0;
   SCIP_Real wtx = 0;

   for(int s=0; s<*nsols; s++)
   {
      SCIP_SOL* sol;
      SCIP_CALL( SCIPcreateSol(scip, &sol, NULL) );
      sol = (*allsolutions)[s] ;
      // SCIP_CALL( SCIPcreateSol(scip, &sol, NULL) );
      fprintf(iter, "================%d=====================\n",s);
      SCIP_CALL(SCIPprintSol((*relaxscip),sol,iter, FALSE ));
      fprintf(iter, "=================%d====================\n",s);
      if(s==0)
      {
         for(int v=0; v<nvars; v++)
         {
            // printf("(%f, %f)",SCIPvarGetObj(relaxvars[v]),SCIPgetSolVal(*relaxscip,(*relaxsolution),relaxvars[v]));
            measurelowerbound+=SCIPvarGetObj(relaxvars[v])*SCIPgetSolVal(*relaxscip,(*relaxsolution),vars[v]);
         }
         printf("\n measure %f and sumofduals %f\n",measurelowerbound,(*sumofduals));
         measurelowerbound+=(*sumofduals);
         printf("\n new lowerbound %f\n",measurelowerbound);   
      }
      
      // SCIP_CALL(SCIPtranslateSubSol(scip,*relaxscip,SCIPgetBestSol(*relaxscip),NULL,relaxvars,&(*relaxsolution)));
      // SCIP_CALL(SCIPtrySol(scip,(*relaxsolution),FALSE,TRUE,TRUE,TRUE,TRUE,&valid));
      // if( (valid) )
      // {
      //    fprintf(iter,"++++++++++++++++=  this solution is feasible. +++++++++++++++++++++++++++++++++++++++++++++++++ \n");
      //    *lowerbound = SCIPgetPrimalbound(scip);
      //    // *result = SCIP_SUCCESS;
      // }
      // else
      // {continue;}

      for(int v=0;v<nvars;v++)
      {
         
         /*first we get the subgradient from the best solution*/
         if(SCIPgetSolVal(*relaxscip,sol,relaxvars[v])==0)
         {
            // fprintf(iter, "varid %d has sol zero %f\n",v,SCIPgetSolVal(*relaxscip,sol,relaxvars[v]));
            feasol[v]=-1;  //we assign initial values equal to -1, signifying that the variable's value hasn't been decided yet.
         }
         else
         {
            wtx += weights[v];
            // fprintf(iter, "varid %d has sol one %f\n",v,SCIPgetSolVal(*relaxscip,sol,relaxvars[v]));
            feasol[v]=2;  //we assign initial values equal to 2, signifying that the variable's value has been passed from the solution.
         } 
      }
      
      // SCIPgetFeasUpperbound(scip,&vardata, &VARSinsidegoodconss, &feasol, nSlotConss, increasingorder, listnconsvars, listconsvarids, arrangedgoodconss, &iter, weights, &(*objval),&sol);

   }
   
   printf("\n================================upperbound %f",wtx);
   (*sumofduals)=0;
   SCIP_Real compare = SCIPinfinity(scip);
   SCIP_Real stepsize = 1;
   compare = 0;
   for(int r=0; r<nSlotConss; r++)
   {
      // 
      (*subgradients)[r]= 1.0;
      for(int t=increasingorder[r]-listnconsvars[r]; t<increasingorder[r]; t++)
      {
         SCIP_VAR* var = relaxvars[listconsvarids[t]];

         // printf("(%s, %d,%f)\t",SCIPvarGetName(var), increasingorder[r]-listnconsvars[r],SCIPgetSolVal(*relaxscip,(*relaxsolution),relaxvars[listconsvarids[t]]) );

         (*subgradients)[r]=(*subgradients)[r]-SCIPgetSolVal(*relaxscip,(*relaxsolution),relaxvars[listconsvarids[t]]);
      }
      compare += ((*subgradients)[r])*((*subgradients)[r]);
      // fprintf(iter,"\n subgrad = %f\n",(*subgradients)[r]);
      // printf("comare-----%f\n",compare);
   }

   stepsize = (90-measurelowerbound)/compare;
  
   printf("\t \t \t stepsize %f \n",stepsize);

   compare = 0 ;
   for(int r=0; r<nSlotConss; r++)
   {   
      (*dualmultipliers)[r]=(*dualmultipliers)[r]+(*subgradients)[r]*stepsize;
      if((*dualmultipliers)[r]<0){(*dualmultipliers)[r]=0;}
      (*sumofduals)+=(*dualmultipliers)[r];
      fprintf(iter,"\t duals = %f\n",(*dualmultipliers)[r]);
   }

   
   printf("=================================Greatlowerbound===============================%f \n",measurelowerbound);
   // if(checker==0){printf("final solution and lowerbound found");return SCIP_SUCCESS;}    

   
      
   return measurelowerbound;
}



// Function to print the output
void printTheArray(int arr[], int n)
{
    for (int i = 0; i < n; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

// Function to generate all binary strings
void generateAllBinaryStrings(int n, int** arr, int i,int index)
{
    if (i == n) {
        printTheArray((*arr), n);
        return;
    }
 
    // First assign "0" at ith position
    // and try for all other permutations
    // for remaining positions
    (*arr)[i] = 2;
    generateAllBinaryStrings(n, &(*arr), i + 1, index);
    (*arr)[index] = 2;
    // And then assign "1" at ith position
    // and try for all other permutations
    // for remaining positions
    (*arr)[i] = -1;
    generateAllBinaryStrings(n, &(*arr), i + 1,index);
}








/** solving process initialization method of relaxator (called when branch and bound process is about to begin) */
#if 0
static
SCIP_DECL_RELAXINITSOL(relaxInitsollagr)
{  /*lint --e{715}*/
   SCIPerrorMessage("method of lagr relaxator not implemented yet\n");
   SCIPABORT(); /*lint --e{527}*/

   return SCIP_OKAY;
}
#else
#define relaxInitsollagr NULL
#endif


/** solving process deinitialization method of relaxator (called before branch and bound process data is freed) */
#if 0
static
SCIP_DECL_RELAXEXITSOL(relaxExitsollagr)
{  /*lint --e{715}*/

   SCIPerrorMessage("method of lagr relaxator not implemented yet\n");
   SCIPABORT(); /*lint --e{527}*/

   return SCIP_OKAY;
}
#else
#define relaxExitsollagr NULL
#endif


/** execution method of relaxator */
static
SCIP_DECL_RELAXEXEC(relaxExeclagr)
{  
   printf("hellow\n");
   SCIP* relaxscip;
   SCIP_HASHMAP* varmap;
   SCIP_HASHMAP* consmap;

   SCIP_PROBDATA* probdata;
   SCIP_VARDATA* vardata;
   SCIP_RELAXDATA* relaxdata;
   
   SCIP_Bool valid;

   /*initialize relaxdata*/
   SCIP_CALL( SCIPallocBlockMemory(scip, &relaxdata) );

   *lowerbound = -SCIPinfinity(scip);
   *result = SCIP_SUCCESS;

   /* we can only run if none of the present constraints expect their variables to be binary or integer during transformation */
   SCIP_CONS** conss = SCIPgetConss(scip);
   int nconss = SCIPgetNConss(scip);


   /**************************************************************************************************************/
   /*First,                                                                                                      */
   //*the probdata: where we get to identify the bad constraint we want to formulate(in our case, the slot conss) */
   /***************************************************************************************************************/
   SCIP_VAR** vars = SCIPgetVars(scip);
   SCIP_VAR** varbuffers;  //will be used for saving the variables contained in a single constraint. The size will be determined using scipgetnconsvars. 
   int* badconss;       //will be used for saving the rows that are to be relaxed.
   int* goodconss;      //will be used for saving the rows that will remain as a constraint.
   int nvars = SCIPgetNVars(scip);

   int* saveindexes; 
   SCIP_CALL(SCIPallocBufferArray(scip, &saveindexes, nvars));
   for(int v = 0; v < nvars; v++)
   {
      saveindexes[v]=SCIPvarGetIndex(vars[v])-SCIPvarGetIndex(vars[0]);
      // printf("(%s %d)\n", SCIPvarGetName(vars[v]),saveindexes[v]);
   }
   

   SCIPcreateprobdata(scip,&probdata,&varbuffers,&badconss,&goodconss,saveindexes);     /*will be used to identify the # of slot(bad) constraints*/ 
   int nSlotConss = SCIPgetNSlotConss(probdata);         // Number of bad constrains. To change this, only change the sign of the condition. eg. 36
   int allnconsvars = SCIPgetallnconsvars(probdata);    //sum of all nconsvars, used later for creating an array that collects the list of varids in each row. here all = 99
   int* listnconsvars = SCIPlistnconsvars(probdata);     // 2  2  2  2  2  2  2  2  2  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3 
   int* listconsvarids = SCIPlistconsvarids(probdata);   //contains in order the list of varindexes found in each bad constraint: (non zero coloums in each row). Examples {0,10,1,11,2,12,...}
   int* increasingorder = SCIPlistincreasing(probdata);  // 2  4  6  8  10  12  14  16  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63  66  69  72  75  78  81  84  87  90  93  96  99
   int* storenconsvars = SCIPprobdataGetStorenconsvars(probdata) ;                   //will store the nconsvars in good constraints in an increasing order, eg. 9 9 9 9 9 9 9 
   int* arrangedgoodconss = SCIPprobdataGetArrangedgoodconss(probdata);                // will store the arranged cons ids(row numbers) according to the nconsvars. eg, 37,38,39,40,41,42,43
   int* inwhichgoodconss = SCIPVarinwhichgoodconss(probdata);                   //containts which good row a var is found in [F1, F1, F1, F1, F1, F1, F2, F2, ...]
 
   int* consids;
   SCIP_CALL(SCIPallocBufferArray(scip,&consids,nSlotConss));  //consids can be considered the array containing non-zero entries in each column. 

   FILE* varobjects;
   varobjects=fopen("varobjs.txt","wr");

   /**************************************************************************************************************/
   /*second, adding the vardata for each variable. This is the column information. The vardata contains informations like:*/
   /*   - Where in the relaxed constraint is this variable found. ex. for var1, row 0 and 10.                     */
   /*   - the index of each variable, just in case their original index was changed, and                          */
   /*   - how many rows in total this variable is fund in                                                         */
   /***************************************************************************************************************/
   SCIPcreateVarData(scip,vars,varbuffers,nSlotConss,badconss,consids,vardata,varobjects);
   /*let's arrange the var index in accordance with the incrasing order of nbad rows in which the var is found in. */
   int* nbadrowsvarisin;
   SCIP_CALL(SCIPallocBufferArray(scip,&nbadrowsvarisin,nvars));
   for(int v=0; v<nvars; v++)
   {
      nbadrowsvarisin[v]=SCIPvarGetIndex(vars[v])-SCIPvarGetIndex(vars[0]);
   }

   for(int v = nvars-1; v>=0; --v)
   {
      for(int t=0; t<v; ++t)
      {
         int locator = 0;
         if(SCIPvardataGetnbadconssVarisin(SCIPvarGetData(vars[nbadrowsvarisin[t]]))>SCIPvardataGetnbadconssVarisin(SCIPvarGetData(vars[nbadrowsvarisin[t+1]])))
         {
            // printf("%d->",SCIPvardataGetnbadconssVarisin(SCIPvarGetData((vars)[t])));
            locator = nbadrowsvarisin[t];
            nbadrowsvarisin[t]=nbadrowsvarisin[t+1];
            nbadrowsvarisin[t+1]=locator;
         }
      }
   } 


   /*Next, we create a feasible solution so we can find an upperbound*/
   int* feasol;
   SCIP_CALL(SCIPallocBufferArray(scip,&feasol,nvars));               //will containt a feasible solution for our problem.

   SCIP_Real* weights;
   SCIP_CALL(SCIPallocBufferArray(scip,&weights,nvars));              //will contain the delay(weight) of each variable in the original problem.
   for(int v=0;v<nvars;v++)
   {
      weights[v] = SCIPvarGetObj(vars[v]);
      feasol[v]=2;  //we assign initial values equal to -1, signifying that the variable's value hasn't been decided yet.
   }
   FILE* feasolfile;
   feasolfile=fopen("feasol.txt","wr");

   SCIP_VAR** VARSinsidegoodconss;             //will contain the variable in a start constraint
   SCIP_CALL(SCIPallocBufferArray(scip, &VARSinsidegoodconss, storenconsvars[nconss-nSlotConss-1]));          //the size is the max num a start constraint can hold. We do this not to allocate memory over and over again. 

   //initialize this allocation
   for(int v=0; v<storenconsvars[nconss-nSlotConss-1]; v++)
   {
      VARSinsidegoodconss[v]=SCIPgetVars(scip)[0];
   }

   SCIP_SOL* mysol;
   SCIP_Real objval = SCIPinfinity(scip);
   SCIP_CALL(SCIPclearRelaxSolVals(scip,relax));
   SCIP_CALL( SCIPcreateSol(scip, &relaxdata->sol, NULL) );

   /* create working solution */
   // SCIPgetFeasUpperbound(scip,&vardata, &VARSinsidegoodconss, &feasol, nSlotConss, increasingorder, listnconsvars, listconsvarids, arrangedgoodconss, &feasolfile, weights, &objval,&mysol);


   

   /*The following is a way for generating another solution, by first creating an array that satisfies the good constraints*/
   /*beginning of algorithm*/
   int* anotherfeasol;
   int nconsvars = 0;
   SCIP_CALL(SCIPallocBufferArray(scip,&anotherfeasol,nvars));   
   SCIP_SOL* anothersol;
   SCIP_Real anotherobjval = objval;
   SCIP_Real lower;
   printf("\n");
   FILE* lowerboundfile;
   lowerboundfile=fopen("lowerboundfile.txt","wr");

   for(int v=0;v<nvars;v++)
   {  
      anotherfeasol[v]=-1; 
   }

   printf("\nlower = %f\n",lower);
   // SCIPgetFeasUpperbound(scip,&vardata, &VARSinsidegoodconss, &anotherfeasol, nSlotConss, increasingorder, listnconsvars, listconsvarids, arrangedgoodconss, &lowerboundfile, weights, &anotherobjval,&mysol);
   printf("\n");

   printf("upperbound ________%f",SCIPgetPrimalbound(scip));
   SCIP_Real upperbound = SCIPgetPrimalbound(scip);

   /******************************************************************************************************/
   /*next step will be creating a relaxscip instance, along with the vardata and other important things. */
   /******************************************************************************************************/
   /* create the variable mapping hash map */
   SCIP_CALL( SCIPcreate(&relaxscip) );
   SCIP_CALL( SCIPhashmapCreate(&varmap, SCIPblkmem(relaxscip), SCIPgetNVars(scip)));
   valid = FALSE;
   SCIP_CALL( SCIPcopy(scip, relaxscip, varmap, consmap, "relaxscip", FALSE, FALSE, FALSE, FALSE, &valid) );

   SCIP_VAR** relaxvars = SCIPgetVars(relaxscip);
   SCIP_CONS** relaxconss = SCIPgetConss(relaxscip);
   /*relaxation step: we remove the bad conss(relaxed conss)*/
   for(int r=0;r<nSlotConss;r++)
   {
      int id = badconss[r];
      SCIP_CONS* cons = SCIPgetConss(relaxscip)[id];
      SCIP_CALL(SCIPdelCons(relaxscip,cons));
   }
   
   FILE* iter;
   iter=fopen("iter.txt","wr");
   FILE* dual;
   dual=fopen("dual.txt","wr");
   FILE* subgrad;
   subgrad=fopen("subgrad.txt","wr");
   // FILE* lowerboundfile;
   // lowerboundfile=fopen("lowerboundfile.txt","wr");
   FILE* solvingfile;
   solvingfile = fopen("solvingfile.txt","wr");

   /******************************************************************************************************************/
   /*Next, we will do the initial iteration of finding the dual mulpliers of each slot conss, and their sum(dualsum) */
   /* In the end, we will subtract this sum from the objective of the function.                                      */
   /* It's initial, because while we would search for more dual multipliers to solve the Lagrangian relaxation       */
   /******************************************************************************************************************/
   SCIP_Real* dualmultipliers;
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&dualmultipliers,nSlotConss));
   
   SCIP_Real* subgradients;
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&subgradients,nSlotConss));

   SCIP_Real* adjustmentoffice;
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&adjustmentoffice,nSlotConss));
      
   
   SCIP_Real* relaxsolval;
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&relaxsolval,nvars));               //will containt a feasible solution for our problem.
   
   SCIP_SOL** allsolutions;
   SCIP_SOL* relaxsolution;
   SCIP_CALL( SCIPcreateSol(scip, &relaxsolution, NULL) );
   int nsols = 0;
   int* feasbestsol;//will be used for generating feasible solution from the best solution.
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&relaxsolval,nvars));

   
   SCIP_Real stepsize = 1;
   SCIP_Real measurelowerbound = *lowerbound;
   //initialize subgradients;
   SCIP_Real sumofduals=0;

   for ( int r = 0; r < nSlotConss; ++r)
   {
      // printf("\n");
      dualmultipliers[r] = 0;
      adjustmentoffice[r] = 1; 
      sumofduals+=dualmultipliers[r];                    //adds the negative of the minimum in each iteration
   }

   int maxiter=10;
   for(int k=0; k<maxiter; ++k)
   {

      /*add the weight to the variables using the dualmultipliers, and then solve the problem */
      measurelowerbound = sciprelaxsolve(scip,&relaxscip,vardata,VARSinsidegoodconss,&sumofduals,&relaxsolval,&relaxsolution,&allsolutions,&nsols,feasol,nSlotConss,increasingorder,listnconsvars,listconsvarids,arrangedgoodconss,inwhichgoodconss, weights,&objval,iter,&(*lowerbound),&subgradients,&dualmultipliers,upperbound,k);   
      if(measurelowerbound>*lowerbound){*lowerbound=measurelowerbound;}
      fprintf(dual, "%f\n",measurelowerbound);
      // sumofduals=0;
      // for ( int r = 0; r < nSlotConss; ++r)
      // {
      //    int id = 
      //    subgradients[r] = -1;

      //    sumofduals+=dualmultipliers[r];                    //adds the negative of the minimum in each iteration
      // }
      // SCIPsetdualmultipliers(scip,relaxscip,vardata,arrangedgoodconss,anotherfeasol,nSlotConss,stepsize, relaxsolution,&dualmultipliers,adjustmentoffice,weights,&sumofduals,VARSinsidegoodconss,&dual);

      /*now we go back to the original relaxed problem, so we can repeat the above process again*/
      SCIP_CALL( SCIPfreeTransform(relaxscip) );
      SCIP_CALL( SCIPtransformProb(relaxscip) );

      for (int v = 0; v<nvars; ++v)
      {
         SCIP_CALL(SCIPchgVarObj(relaxscip,relaxvars[v],weights[v]));       
      }

      SCIP_CALL( SCIPfreeTransform(relaxscip) );

   }


   /* free memory */
   fclose(solvingfile);
   fclose(lowerboundfile);
   fclose(subgrad);
   fclose(dual);
   fclose(iter);
   fclose(feasolfile);   
   fclose(varobjects);
   SCIPhashmapFree(&varmap);
   SCIP_CALL( SCIPfree(&relaxscip) );
   return SCIP_OKAY;
}






/*
 * relaxator specific interface methods
 */

/** creates the lagr relaxator and includes it in SCIP */
SCIP_RETCODE SCIPincludeRelaxlagrangian(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_RELAXDATA* relaxdata;
   SCIP_RELAX* relax;

   /* create lagr relaxator data */
   SCIP_CALL(SCIPallocMemory(scip, &relaxdata));
   relaxdata = NULL;
   /* TODO: (optional) create relaxator specific data here */

   relax = NULL;

   /* include relaxator */

   SCIP_CALL( SCIPincludeRelaxBasic(scip, &relax, RELAX_NAME, RELAX_DESC, RELAX_PRIORITY, RELAX_FREQ,
         relaxExeclagr, relaxdata) );

   assert(relax != NULL);

   /* set non fundamental callbacks via setter functions */
   // SCIP_CALL( SCIPsetRelaxCopy(scip, relax, relaxCopylagr) );
   SCIP_CALL( SCIPsetRelaxFree(scip, relax, relaxFreelagr) );
   SCIP_CALL( SCIPsetRelaxInit(scip, relax, relaxInitlagr) );
   SCIP_CALL( SCIPsetRelaxInitsol(scip, relax, relaxInitsollagr) );
   SCIP_CALL( SCIPsetRelaxExitsol(scip, relax, relaxExitsollagr) );

   /* add lagr relaxator parameters */

   /* TODO: (optional) add relaxator specific parameters with SCIPaddTypeParam() here */

   return SCIP_OKAY;
}
