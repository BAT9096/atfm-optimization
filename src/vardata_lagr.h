/**@file    relax_lagr.cpp
 * @ingroup Lagrangian
 * @brief   lagrangian relaxation
 * @author  Dawit Hailu
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_VARDATA_LAGR__
#define __SCIP_VARDATA_LAGR__

#include "scip/scip.h" 

/** create variable data, in the future I will add the delay array and delay of each var*/
SCIP_RETCODE SCIPvardataCreateLagrangian(
   SCIP*                            scip,                          /**< SCIP data structure*/
   SCIP_VARDATA*                    vardata,                       /**<pointer to the vardata*/
   SCIP_VAR**                       vars,
   // SCIP_CONS**                      VarSlotConss,                  /**< all slot constraints containing the variable */
   // int                              nVarSlotConss,                 /**<number of slot constraints the variable is occuring in*/
   int                              nSlotConss,
   int                              v

);


SCIP_RETCODE SCIPvaridentifier(
   SCIP*                            scip,                          /**< SCIP data structure*/
   SCIP_VARDATA*                   vardata,                       /**<pointer to the vardata*/
   SCIP_VAR**                       var,
   SCIP_VAR**                       varbuffers,
   int*                           consids,
   int*                           badconss,
   int                              nSlotConss
);
/** gets the slot conss the var is occuring*/
SCIP_CONS** SCIPvardataGetSlotConss(
	SCIP_VARDATA* vardata     /**< variable data */
   );

/** gets the number of slot conss the var is occuring in*/
int SCIPvardataGetnVarSlotConss(
	SCIP_VARDATA* vardata     /**< variable data */
);

/** gets the ids of the slotconss the var is occuring*/
int* SCIPvardataGetconsids(
	SCIP_VARDATA* vardata     /**< variable data */
);

SCIP_Real SCIPvarGetQuotient(
   SCIP_VARDATA* vardata
);

SCIP_RETCODE vardataDelete(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VARDATA**        vardata             /**< vardata to delete */
   );
/** prints vardata to file stream */
void SCIPvardataPrint(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VARDATA*         vardata,            /**< variable data */
   FILE*                 file                /**< the text file to store the information into */
);


SCIP_RETCODE vardataCreate(
   SCIP*                scip,                     /**< SCIP data structure */
   SCIP_VARDATA**       vardata,                  /**< pointer to vardata */
   SCIP_CONS**          VarSlotConss,             /**< array of constraints ids */
   int                  nVarSlotConss,             /**< number of constraints */
   SCIP_Real            varquotient
);  

/** frees user data of variable */
SCIP_RETCODE vardatafree(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VARDATA**        vardata             /**< vardata to delete */
   );

SCIP_RETCODE lagrVarObjoverNVarslotConss (
   SCIP_VARDATA*** vardata,
   SCIP_VAR** var
);

SCIP_Real SCIPVaraddDualMultiplier(
   SCIP* scip,
   SCIP_VAR** var,
   SCIP_Real* dualmultipliers,
   SCIP_PROBDATA* probdata
);

int SCIPvardataGetVarID(
	SCIP_VARDATA* vardata     /**< variable data */
);

SCIP_RETCODE SCIPvarchangeDuals(
   SCIP* relaxscip,
   SCIP_VAR*** vars, 
   SCIP_Real* dualmultipliers,
   SCIP_Real* origobj
);



#endif
