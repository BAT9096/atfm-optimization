#include "probdata_lagr.h"
#include "vardata_lagr.h"
#include "relax_lagr.h"


#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "relax_lagr.h"
/*Using ProbData as a memory location for the constraints*/
struct SCIP_ProbData
{
   SCIP_CONS**          SlotConss;
   SCIP_CONS**          StartConss;
   int*                 varids;                      /**<Ids for the variables found in the slot constraint*/
   int                  nSlotConss;                   // number of slot constraints.
   int                  nStartConss;                  // number of start constraints.
   int                  nbadconss;
   int                  ngoodconss;
   int*                 badconss;
   int*                 goodconss;
   int                  allnconsvars;
   int*                 listnconsvars;
   int*                 listconsvarids;
   int*                 increasingorder;
   int*                 storenconsvars;                   //will store the nconsvars in good constraints in an increasing order
   int*                 arrangedgoodconss;                // will store the arranged cons ids(row numbers) according to the nconsvars
   int*                 inwhichgoodconss;

};

int* SCIPslotgetvarids(
SCIP_ProbData* probdata
){
    return probdata->varids;
 }
/*list the number of variable each bad constraint has. ex. 2  2  2  2  2  2  2  2  2  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3 */
int* SCIPlistnconsvars(
SCIP_ProbData* probdata
){
    return probdata->listnconsvars;
 }
 /*contains in order the list of varindexes found in each bad constraint: (non zero coloums in each row). Examples {0,10,1,11,2,12,...}*/
int* SCIPlistconsvarids(
SCIP_ProbData* probdata
){
    return probdata->listconsvarids;
 }
/*will store the nconsvars in good constraints in an increasing order*/ 
int* SCIPprobdataGetStorenconsvars(
SCIP_ProbData* probdata
){
    return probdata->storenconsvars;
 }

  /*will store the arranged cons ids(row numbers) according to the nconsvars*/
int* SCIPprobdataGetArrangedgoodconss(
SCIP_ProbData* probdata
){
    return probdata->arrangedgoodconss;
 }

  /* puts the SCIPlistnconsvars in increasing order: ex. 2  4  6  8  10  12  14  16  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63  66  69  72  75  78  81  84  87  90  93  96  99*/
int* SCIPlistincreasing(
SCIP_ProbData* probdata
){
    return probdata->increasingorder;
 }

/* Number of bad constrains. To change this, only change the sign of the condition. eg. 36*/
int SCIPgetNSlotConss(
SCIP_ProbData* probdata
){
    return probdata->nSlotConss;
 }
/*sum of all nconsvars, used later for creating an array that collects the list of varids in each row. here all = 99*/
int SCIPgetallnconsvars(
SCIP_ProbData* probdata
){
       return probdata->allnconsvars;
   }
/*in which good conss is a variable found in*/
int* SCIPVarinwhichgoodconss(
SCIP_ProbData* probdata
){
    return probdata->inwhichgoodconss;
 }

SCIP_RETCODE GetNGoodandNbad(
   SCIP* scip,
   int* nbad,
   int* ngood,
   SCIP_PROBDATA** probdata

){
   // int nbad=0;
   // int ngood=0;
   SCIP_Bool success;
   SCIP_CONS** conss = SCIPgetConss(scip);
   for(int r=0; r<SCIPgetNConss(scip); r++)
   {
      SCIP_CONS* cons = conss[r];
      
      //Now we add the condition(criteria) for separating the good and bad constraints. Let's first try inequality as a bad constraint
      //first we get the number of good and bad constraints.

      if(SCIPconsGetLhs(scip,cons,&success)==-SCIPinfinity(scip))                        /*<We get the slot constraints based on the inquality*/
      {     
         ++(*nbad);
      }

      else
      {
         ++(*ngood);
      }
   } 
   printf("\n%d, %d, %d\n",*nbad, *ngood, SCIPgetNConss(scip));
   assert(nbad+ngood=SCIPgetNConss(scip));
   // SCIP_CALL(SCIPallocMemory(scip,probdata));
   // (*probdata)->nbad = *nbad;
   // (*probdata)->ngood = *ngood;
   //* we now store the ids of the constraints classified as good or bad. 


   return SCIP_OKAY;
}


/* here we create the probdata, which will be called for storage of values to the data*/
SCIP_RETCODE probdataCreate(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_PROBDATA**       probdata,           /**< pointer to problem data */
   SCIP_CONS**          SlotConss,
   // SCIP_CONS**          StartConss,
   int                   nSlotConss,
   int                   nStartConss   
   )       /**< number of slot constraints */
{
   assert(scip != NULL);
   assert(probdata != NULL);
   assert(SlotConss!=NULL);
   assert(nStartConss >= 0);
   assert(nSlotConss >= 0);

   /* allocate memory */
   SCIP_CALL( SCIPallocBlockMemory(scip, probdata) );
   // BMSclearMemory(*probdata);

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*probdata)->SlotConss, nSlotConss) );
   // BMSclearMemoryArray((*probdata)->SlotConss, nSlotConss);
   /* duplicate memory*/
   SCIP_CALL(SCIPduplicateBlockMemoryArray(scip, &(*probdata)->SlotConss, SlotConss, nSlotConss));           
   (*probdata)->nSlotConss = nSlotConss;
   (*probdata)->nStartConss = nStartConss;

   return SCIP_OKAY;
}

/* here we FREE the probdata*/
SCIP_RETCODE probdataFree(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_PROBDATA**       probdata           /**< pointer to problem data */
   )
{
   assert(scip != NULL);
   assert(probdata != NULL);
   assert(*probdata!=NULL);

   /* free memory */
   if((*probdata)->SlotConss != NULL)
   {
      SCIPfreeBlockMemoryArray(scip, &(*probdata)->SlotConss, (*probdata)->nSlotConss);
   }
   if((*probdata)->StartConss != NULL)
   {
      SCIPfreeBlockMemoryArray(scip, &(*probdata)->StartConss, (*probdata)->nStartConss);
   }
   SCIPfreeBlockMemory(scip, probdata);
   
   return SCIP_OKAY;
}


/*We will create the probdata to have the slot constraints and the variables they hold withn them*/

SCIP_RETCODE SCIPcreateprobdata
(   SCIP*                  relaxscip,
    SCIP_ProbData**        probdata,
    SCIP_VAR***            varbuffers,  
    int**                   badconss,        //will contain the ids of those rows that will be relaxed. 
    int**                  goodconss,        //will contain the number of variable in each row of the good constraints, i.e. those that won't be relaxed. 
    int*                   saveindexes 
)
{
   FILE* prob;
   prob = fopen("probdata.txt","wr");
   int nconss = SCIPgetNConss(relaxscip);
   

   int nSlotConss = 0;
   int ngoodconss = 0;
   int id =0;
   SCIP_VAR** vars = SCIPgetVars(relaxscip);
   SCIP_CONS** conss = SCIPgetConss(relaxscip);
   SCIP_Bool success;
   for (int t=0; t<nconss; t++)                             /* (3) */
   {
      SCIP_CONS* cons = conss[t];
      if(SCIPconsGetLhs(relaxscip,cons,&success)==1)                        /*<We get the slot constraints based on the inquality*/
      {
         ++nSlotConss;  
      }
      else
      {
         ++ngoodconss;
      }
   }
   fprintf(prob,"%d nslot conss, %d nstart conss\n",nSlotConss, ngoodconss);

   int allnconsvars=0;
   int nconsvars=0;
   int counter = 0; 
   int goodcounter = 0;
   int maxnconsvars = 0;            //to get the highest number of non-zero entries in the entire relaxed matrix. 
   int* storenconsvars;
   int* arrangedgoodconss;
   SCIP_CALL(SCIPallocBufferArray(relaxscip, badconss, nSlotConss));         //the badconss array will contain the row number of the bad conss  
   SCIP_CALL(SCIPallocBufferArray(relaxscip, goodconss, ngoodconss));
   SCIP_CALL(SCIPallocBufferArray(relaxscip, &storenconsvars, ngoodconss));
   SCIP_CALL(SCIPallocBufferArray(relaxscip, &arrangedgoodconss, ngoodconss));

   /*
      we first get the row numbers of the bad conss, and save to the array: badconss.
   */
   for(int r = 0; r<nconss; ++r)
   {
      SCIP_CONS* cons = conss[r];
      
      if(SCIPconsGetLhs(relaxscip,cons,&success)==1)                        /*<We get the slot constraints based on the inquality*/
      {
         (*badconss)[counter]=r;
         SCIP_CALL(SCIPgetConsNVars(relaxscip,cons,&nconsvars,&success));
         counter++;
         if(maxnconsvars<nconsvars){maxnconsvars=nconsvars;}
         allnconsvars+=nconsvars;
      }

      else
      {         
         SCIP_CALL(SCIPgetConsNVars(relaxscip,cons,&nconsvars,&success));
         storenconsvars[goodcounter]=nconsvars;
         (*goodconss)[goodcounter]=r;
         arrangedgoodconss[goodcounter]=r;
         goodcounter++;
         fprintf(prob, "%d ",nconsvars);
      }
   }
   fprintf(prob,"\n");

   //we now arrange the array in an increasing order, which will help us later on in creating a feasible solution. 
   for(int r = ngoodconss-1; r>=0; --r)
   {
      for(int t=0; t<r; ++t)
      {
         int temp = 0;
         int locator = 0;
         if(storenconsvars[t]>storenconsvars[t+1])
         {
            temp = storenconsvars[t];
            storenconsvars[t]=storenconsvars[t+1];
            storenconsvars[t+1]=temp;

            locator = arrangedgoodconss[t];
            arrangedgoodconss[t]=arrangedgoodconss[t+1];
            arrangedgoodconss[t+1]=locator;
         }
      }
   }    

   SCIP_VAR** VARSinsidegoodconss;             //will contain the variable in a start constraint
   SCIP_CALL(SCIPallocBufferArray(relaxscip, &VARSinsidegoodconss, storenconsvars[nconss-nSlotConss-1]));          //the size is the max num a start constraint can hold. We do this not to allocate memory over and over again. 

   //initialize this allocation to contain all sizes of elements of the start constraint
   for(int v=0; v<storenconsvars[nconss-nSlotConss-1]; v++)
   {
      VARSinsidegoodconss[v]=SCIPgetVars(relaxscip)[0];
   }

   int* inwhichgoodconss;
   SCIP_CALL(SCIPallocBufferArray(relaxscip, &inwhichgoodconss, SCIPgetNVars(relaxscip)));          

   //we now make create an array that containts the row number(in the good conss) of the variable. 
   for(int r = 0; r<nconss; ++r)
   {
      SCIP_CONS* cons = conss[r];
      
      if(SCIPconsGetLhs(relaxscip,cons,&success)==-SCIPinfinity(relaxscip))                        /*<We get the slot constraints based on the inquality*/
      {
         fprintf(prob,"%s->",SCIPconsGetName(cons));
         SCIP_CALL(SCIPgetConsNVars(relaxscip, cons, &nconsvars, &success));
         SCIP_CALL(SCIPgetConsVars(relaxscip, cons, VARSinsidegoodconss, nconsvars, &success));
         if (!success){abort(); }
         for (int j = 0; j < nconsvars; ++j)                                            /* (8) */
         {
            SCIP_VAR* varx = VARSinsidegoodconss[j];
            int index = SCIPvarGetIndex(varx)-SCIPvarGetIndex(vars[0]);
            fprintf(prob,"(%s in %d)", SCIPvarGetName(varx),index);
            inwhichgoodconss[index]=r;
         }
         fprintf(prob,"\n");
      }
   }

   for(int v=0; v<SCIPgetNVars(relaxscip);v++)
   {
      // printf("(%s in %s)\n", SCIPvarGetName(vars[v]), SCIPconsGetName(conss[inwhichgoodconss[v]]));  
   }

   /*to get number of all possible solutions, given */
   /*
   int npossibilities = 0;
   for(int r = 0; r<ngoodconss; ++r)
   {
      fprintf(prob," %d \t %d \n ",storenconsvars[r], arrangedgoodconss[r]);
      npossibilities += (int)pow(2,storenconsvars[r]);
      printf("(%d,%d)->",storenconsvars[r], npossibilities);

   }
   
    printf("in total = %d)->", npossibilities);
   */
   /*
      Our first objective is to create an array, containing the non-zero variables 
      found in each bad conss. But instead of listing them in multiple rows, we list them just in one. 
      for example: slot1 have vars[0] and vars[10] non-zero and slot2 has vars[1] and vars[11]. 
      listconsvarids = {0,10,1,11, ...}
   */
   int* listconsvarids;   //examples {0,10,1,11,2,12,...} we use the 
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&listconsvarids,allnconsvars));
   
   /*
      we save the number of non-zero variables in bad constraint has.
   */
   int* listnconsvars;  //example {2,2,2,3,3,3}
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&listnconsvars,nSlotConss));
   
   /*to not allocate buffer array over and over again, we create one, with
   size = maxnconsvars. and then we intialize it with the first few variables from vars. 
   */
   SCIP_CALL(SCIPallocBufferArray(relaxscip, varbuffers, maxnconsvars));
   for (int v = 0; v < maxnconsvars; v++)
   {
      (*varbuffers)[v] =vars[v];
   }

   fprintf(prob,"all = %d\n",allnconsvars);
   
   counter=0;
   for (int r = 0; r < nSlotConss; ++r)
   {
      nconsvars=0;
      id = (*badconss)[r];
      SCIP_CONS* cons = conss[id];

      // printf("%s \t",SCIPconsGetName(cons));
      SCIP_CALL(SCIPgetConsNVars(relaxscip, cons, &nconsvars, &success)); 
      SCIP_CALL(SCIPgetConsVars(relaxscip, cons, (*varbuffers), nconsvars, &success));
      if (!success){
      abort(); }
      listnconsvars[r] = nconsvars;
      fprintf(prob," %d ",listnconsvars[r]);
      for (int j = 0; j < nconsvars; ++j)                                            /* (8) */
      {
         int varbufindex = SCIPvarGetIndex((*varbuffers)[j])-SCIPvarGetIndex(vars[0]);
         assert(varbufindex != NULL);
         listconsvarids[counter]=varbufindex;
         fprintf(prob,"%s ",SCIPvarGetName((*varbuffers)[j]));
         
         counter++;
      }
   
   }
   
   int* increasingorder; //example {2,4,6,9,12,...}
   SCIP_CALL(SCIPallocBufferArray(relaxscip,&increasingorder,nSlotConss));
   increasingorder[0] = listnconsvars[0]; //listnconsvars[0];
   fprintf(prob,"\n Increasing order :\n");
   for(int r=1; r<nSlotConss;r++)
   {
      increasingorder[r]=listnconsvars[r]+increasingorder[r-1];
   }
   fprintf(prob,"\n");

   

   
   //printing to prob.txt
   /*
   for(int r=0; r<nSlotConss;r++)
   {
      fprintf(prob," %d ",increasingorder[r]);
   }

   for(int r=0; r<nSlotConss;r++)
   {
      for (int p = increasingorder[r]-listnconsvars[r]; p < increasingorder[r]; p++)
      {
         // fprintf(prob,"%s ",SCIPvarGetName(vars[listconsvarids[p]]));
         fprintf(prob,"%s ",SCIPvarGetName(vars[listconsvarids[p]]));
         fprintf(prob,"%d ",listconsvarids[p]);
      }
      fprintf(prob,"\n");
   }
   */

   fclose(prob);

   SCIP_CALL(SCIPallocMemory(relaxscip,probdata));
   (*probdata)->nSlotConss = nSlotConss;
   (*probdata)->allnconsvars= allnconsvars;  //sum of all nconsvars;
   (*probdata)->listnconsvars = listnconsvars;
   (*probdata)->listconsvarids = listconsvarids;
   (*probdata)->increasingorder = increasingorder;
   (*probdata)->arrangedgoodconss = arrangedgoodconss;
   (*probdata)->storenconsvars = storenconsvars;
   (*probdata)->inwhichgoodconss = inwhichgoodconss;

   return SCIP_OKAY;
}


/*************************************************************************************************/
/* This is a function that will take a constraint(slot), and computes the following formula      */
//*     >>dualMultiplier(cons)=min {SCIPvarGetQuotient for each of the variable in the cons}  */
/* the function returns this minimum value                                                       */
/**************************************************************************************************/
SCIP_Real SCIPconsGetMultiplier(SCIP* scip,SCIP_CONS** cons,SCIP_Real subgradient,SCIP_Real C, SCIP_Real stepsize,SCIP_Bool firstiteration, SCIP_Real dualval)
{
   
   SCIP_Real min;
   SCIP_Bool success;
  
   if(SCIPconsGetLhs(scip,*cons,&success)==1)
   {
      if(firstiteration==true)
      {
         min = SCIPinfinity(scip);                                   //*we take a very big number for a comparision that will happen later on(i ust this as I'm not sure how to assing the infinity value in SCIP)                         
         SCIP_VAR** varbuffer;
         
         SCIP_VARDATA*  vardata;    

         int nconsvars;
         SCIP_CALL(SCIPgetConsNVars(scip,*cons,&nconsvars,&success));
         assert(nconsvars!=0);
         SCIP_CALL(SCIPallocBufferArray(scip, &varbuffer, nconsvars));
         SCIP_CALL(SCIPgetConsVars(scip,*cons,varbuffer,nconsvars,&success));
         if (!success)
            abort();
         if(nconsvars==0)
         {
            min = 0;                     //to make sure that if the constraint doesn't have any variables, the dual will be 0. 
         }

         else
         {
            SCIP_Real* compare;
            SCIP_CALL(SCIPallocBufferArray(scip, &compare, nconsvars));
            for(int j=0;j<nconsvars;j++)
            {
               SCIP_VAR* consvar= varbuffer[j];
               vardata = SCIPvarGetData(consvar); 
               compare[j]=SCIPvarGetQuotient(vardata);
               
               if (compare[j]<min)
               {
                  
                  min = compare[j];

               }
            }
            if (min<0){min = 0;}     
         }    
         return min;              
      }

      else
      {
         
         min = dualval + subgradient*stepsize;
         
         if(min < 0)
         {
            //prinf("-ve min %f",min);
            return 0;

         }
         else
         {   
            //prinf("min %f = (subgradients[r]->%f*stepsize->%f *C->%f\n) = ",min,subgradient,stepsize,C);
            return min;
         }
      }
   }
   return SCIP_OKAY;
}
      

SCIP_RETCODE SCIPsetdualmultipliers(
   SCIP* scip,
   SCIP* relaxscip,
   SCIP_VARDATA* vardata,
   int* arrangedgoodconss,
   int* feasol,
   int nSlotConss,
   int stepsize,
   SCIP_SOL* relaxsolution,
   SCIP_Real** dualmultipliers,
   SCIP_Real* adjustmentoffice,
   SCIP_Real* weights,
   SCIP_Real* sumofduals,
   SCIP_VAR** VARSinsidegoodconss,
   FILE** dualfile
){

   SCIP_VAR** vars = SCIPgetVars(scip);
   SCIP_VAR** relaxvars = SCIPgetVars(relaxscip);
   SCIP_CONS** conss = SCIPgetConss(scip);
   (*sumofduals) = 0;
   int nconss = SCIPgetNConss(scip);
   SCIP_Bool valid;

   for(int r=0; r <nconss-nSlotConss; ++r)
   {
      int id = arrangedgoodconss[r];
      int nconsvars = 0;
      SCIP_CONS* cons = SCIPgetConss(scip)[id];
      
      SCIP_CALL(SCIPgetConsNVars(scip,cons,&nconsvars,&valid));
      SCIP_CALL(SCIPgetConsVars(scip, cons, VARSinsidegoodconss, nconsvars, &valid));
      SCIP_Real adjustingnumber=0;
      for(int c=0; c<nconsvars; c++)
      {
         SCIP_VAR* var = VARSinsidegoodconss[c];
         vardata = SCIPvarGetData(var);
         int freq = SCIPvardataGetnbadconssVarisin(vardata);
         if (freq == 0){adjustingnumber=0; continue;}
         int index = SCIPvarGetIndex(VARSinsidegoodconss[c])-SCIPvarGetIndex(vars[0]);
         adjustingnumber = 0;

         //we first check where the solution of a variable has been lost in the feasible solution. 
         if(SCIPgetSolVal(relaxscip,relaxsolution,relaxvars[index])==0 && feasol[index]==1)
         {
            
            adjustingnumber += weights[index];     
            fprintf((*dualfile),"\n %s from ",SCIPvarGetName(vars[index]));
            fprintf((*dualfile),"%f*",weights[index]);
            // continue;

            for(int f=0; f<nconsvars; f++)
            {
               int nextindex = SCIPvarGetIndex(VARSinsidegoodconss[f])-SCIPvarGetIndex(vars[0]);
               if(SCIPgetSolVal(relaxscip,relaxsolution,relaxvars[nextindex])==1 && feasol[nextindex]==0)
               {
                  
                  adjustingnumber -= weights[nextindex];
                  fprintf((*dualfile),"\n to %s %f*",SCIPvarGetName(vars[nextindex]),weights[nextindex]);
                  fprintf((*dualfile),"%f hence difference of %f*",weights[nextindex],adjustingnumber);
                  // continue;
               }   
            }
            adjustingnumber = adjustingnumber/freq;
         }
         //and we go to the variable the solution has become 1. 
       
         else{continue;}
         
         // adjustingnumber = adjustingnumber*adjustingnumber;
         for(int j=0; j<freq; j++)
         {
            int p = SCIPvardataGetconsscontainingvar(vardata)[j];
            // printf("%d with %f",SCIPvardataGetconsscontainingvar(vardata)[j],adjustmentoffice[j]);
            adjustmentoffice[p]=adjustingnumber;
            fprintf((*dualfile),"\n*%s*in**%s**%f**",SCIPconsGetName(cons),SCIPconsGetName(conss[SCIPvardataGetconsscontainingvar(vardata)[j]]),adjustingnumber);
         } 
         adjustmentoffice[index]+=adjustingnumber;
      }
   } 

   fprintf((*dualfile),"\n");
   /**we now use the new method to calculate the dualmultipliers*/
   for(int r=0;r<nSlotConss; ++r)
   {
      if(adjustmentoffice[r]<0){adjustmentoffice[r]=0;}
      
      // adjustmentoffice[r]=sqrt(adjustmentoffice[r]);
      fprintf((*dualfile),"\n%d=>%f and dual multiplier*",r,adjustmentoffice[r]);
      (*dualmultipliers)[r]+=stepsize*adjustmentoffice[r];
      (*sumofduals)+=(*dualmultipliers)[r];
   }
   fprintf((*dualfile),"\n sumofduals = %f",*sumofduals);
   
   return SCIP_OKAY;
}



/******************************************************************************************************************/
/* the next step would be to maximize over the Lagrangian dual Z(dual). This would give us the first iteration     */
//* subgradient^{0}_{r} = sum{x[v]}-1, where x[v] is the solution value of the variables founds in the r-th conss  */
/* the abover formula gives us the 0'th iteration of the subgradiant vector                                        */
/*******************************************************************************************************************/
int SCIPgetSubgradients(
   SCIP* scip,
   int r,
   int* feasol,
   int* listconsvarids,
   int* listnconsvars,
   int* increasingorder

){
   int addsolval = -1;
   for(int t=increasingorder[r]-listnconsvars[r]; t<increasingorder[r]; t++)
   {
      // printf("%d ",feasol[listconsvarids[t]]);
   }
   if(addsolval<0){addsolval=0;}

   return addsolval;
}

              
/*to get a feasible upperbound*/
SCIP_RETCODE SCIPgetFeasUpperbound(
   SCIP* scip,   
   SCIP_VARDATA** vardata,
   SCIP_VAR*** VARSinsidegoodconss,             //an array to contain 
   int** feasol,                                //the exact array with entries to -1 or 2
   int nSlotConss,                              //number of relaxed constraints
   int* increasingorder,
   int* listnconsvars,
   int* listconsvarids,
   int* arrangedgoodconss,
   FILE** fromprob,
   SCIP_Real* weights,
   SCIP_Real* objVal,                               //final objective value in original problem, when multiplied with the original weight. 
   SCIP_SOL** mysol
   )
{
   fprintf((*fromprob),"================================\n");
   SCIP_Bool valid;
   SCIP_VAR** vars=SCIPgetVars(scip);
   int nvars = SCIPgetNVars(scip);
   int nconsvars = 0;
   int nconss = SCIPgetNConss(scip);

   for(int r = 0; r<nconss-nSlotConss;r++)
   {
      int id = arrangedgoodconss[r];
      SCIP_CONS* cons = SCIPgetConss(scip)[id];  
      fprintf((*fromprob),"\n %s, %d\n",SCIPconsGetName(cons), id);
      if(SCIPconsGetLhs(scip,cons,&valid)==1)
      {

         SCIP_CALL(SCIPgetConsNVars(scip, cons, &nconsvars, &valid));
         SCIP_CALL(SCIPgetConsVars(scip, cons, (*VARSinsidegoodconss), nconsvars, &valid));
         if (!(valid)){abort(); }
         for (int j = 0; j < nconsvars; ++j)                                            /* (8) */
         {
            SCIP_VAR* varx = ((*VARSinsidegoodconss))[j];
            *vardata=SCIPvarGetData(varx);
            fprintf((*fromprob),"%s\t",SCIPvarGetName(varx));
            int varbufindex = SCIPvardatagetVarindex(*vardata);  //get the index, which will be used in assigning values to the feassol.
            assert(varbufindex != NULL);
            fprintf((*fromprob),"varbufindex %d\t",varbufindex);  

            if ((*feasol)[varbufindex]!=0)           //if it is -1 or 2 or another thing, depending on what we assigned for it. 
            {
               
               // (*feasol)[varbufindex]=1;
               *vardata=SCIPvarGetData(varx);

               int* varids = SCIPvardataGetconsscontainingvar(*vardata); 
               assert(varids=!NULL);
               int nbadconssVarisin = SCIPvardataGetnbadconssVarisin(*vardata);
               fprintf((*fromprob),"nvarincons = %d\t",nbadconssVarisin);

               if(nbadconssVarisin<=1)   //if the variable isnot constrained by any capacity, it will become 1. 
               {
                  (*feasol)[varbufindex]=1;
               }

               else
               {         
                  for(int t=0;t<nbadconssVarisin;t++)
                  {
            
                     fprintf((*fromprob),"(sid=%d, order=%d, nconsvars=%d,%d) \t",varids[t],increasingorder[varids[t]],listnconsvars[varids[t]],increasingorder[increasingorder[varids[t]]]);
                     
                     // varids[t] gives the constraint address. There we still would like to find out which variables are found.
                     for(int p=increasingorder[varids[t]]-listnconsvars[varids[t]]; p<increasingorder[varids[t]]; ++p)
                     {

                        if(SCIPvarGetIndex(vars[listconsvarids[p]])-SCIPvarGetIndex(vars[0])!=varbufindex)//making sure we are not working on the same variable
                        {
                           fprintf((*fromprob),"var = %s\t",SCIPvarGetName(vars[listconsvarids[p]]));
                           if((*feasol)[listconsvarids[p]]==-1)
                           {
                              fprintf((*fromprob),"turned to 0");
                              (*feasol)[listconsvarids[p]]=0;
                              
                           }
                           else if((*feasol)[listconsvarids[p]]==2)
                           {
                              if(weights[listconsvarids[p]]>=weights[varbufindex])
                              {
                                 fprintf((*fromprob),"of weight = %f\t",weights[listconsvarids[p]]);
                                 (*feasol)[listconsvarids[p]]=0;
                                 
                              }
                              else
                              {
                                 fprintf((*fromprob),"of weight = %f\t",weights[varbufindex]);
                                 // (*feasol)[listconsvarids[p]]=0;
                                 (*feasol)[varbufindex]=0;
                                 break;
                              }
                           }

                           else 
                           {
                              (*feasol)[varbufindex]=1;
                             
                              
                           }
                        }
                     }
                     fprintf((*fromprob),"\n");
                  }  
               }
               if((*feasol)[varbufindex]==0)
               {
                  continue;
               }
               else
               {      
                  for(int s=0; s<nconsvars;++s)
                  {
                     if(s!=j)
                     {
                     
                        SCIP_VAR* varnext = ((*VARSinsidegoodconss))[s];
                        *vardata = SCIPvarGetData(varnext);
                        int varnextindex = SCIPvardatagetVarindex(*vardata);
                        fprintf((*fromprob),"nextvarindex %d \t",varnextindex);
                        (*feasol)[varnextindex]=0;
                     }
                  }
               }
            }
                  
            fprintf((*fromprob),"(%s,%d,%d)\n",SCIPvarGetName(varx), varbufindex,(*feasol)[varbufindex]);
         }
         fprintf((*fromprob),"\n");
      }  
   }

   /*now we save this solution to the solution array given*/

   SCIP_Real upper = SCIPgetPrimalbound(scip);
   SCIP_CALL( SCIPcreateSol(scip, mysol, NULL) );
   
   for(int v=0;v<nvars;v++)
   {
      if((*feasol)[v]==-1||(*feasol)[v]==2){(*feasol)[v]=1;}
      // upper += weights[v]*((*feasol)[v]);
      
      fprintf((*fromprob)," (%s %d weight %f) \n ", SCIPvarGetName(vars[v]),(*feasol)[v], weights[v]);
      SCIPsetSolVal(scip,(*mysol),vars[v],(*feasol)[v]);
   }
   fprintf((*fromprob),"compare: previous %f, current %f",  (*objVal), upper);
   if(upper < (*objVal))
   {
      ( *objVal)=upper;
      fprintf((*fromprob),"objval  in original value will be: %f", (*objVal));
   }
      // SCIP_CALL( SCIPtrySol(scip,(*mysol),TRUE,TRUE, TRUE,TRUE, TRUE,&valid));
   else
   {
      fprintf((*fromprob),"not better upperbound");
   } 
   SCIP_CALL( SCIPtrySol(scip,(*mysol),TRUE,TRUE, TRUE,TRUE, TRUE,&valid));
   // SCIP_CALL( SCIPtrySolFree(scip, mysol, TRUE, FALSE, FALSE, FALSE, FALSE, &valid) );
   SCIP_CALL(SCIPprintSol(scip,(*mysol),*fromprob,FALSE));
   if( (valid) )
   {
      SCIP_CALL( SCIPsetObjlimit(scip, (*objVal)));  
      fprintf((*fromprob),"this heuristic found a solutions \n");
   }
   else
   {
      /* free solution structure, since SCIPtranslateSubSol would recreate in the next round */
      fprintf((*fromprob),"didn't work");
      SCIP_CALL( SCIPfreeSol(scip, mysol ));
      assert(mysol == NULL);
   }
 
   
   return SCIP_OKAY;
}

SCIP_RETCODE scipgetsolutions(SCIP* scip, SCIP_VAR** vars, SCIP_Real** solvals, SCIP_Real* relaxval, SCIP_Real* dualmultipliers, SCIP_Real sumofduals,SCIP_SOL** bestsol)
{
   double sum;
   FILE* varobj;
   varobj=fopen("varobj.txt","wr");
   FILE* problemstate;
   problemstate = fopen("problemstate.txt","w+"); 
   FILE* solutions;
   solutions = fopen("solutions2.txt","w+");

   SCIP_VARDATA* vardata;

   int nvars = SCIPgetNVars(scip);
   for(int v=0;v<nvars;v++)
   {
      SCIP_VAR* var = vars[v];
      sum =SCIPvarGetObj(var);
      
      vardata=SCIPvarGetData(var);
      int* varids = SCIPvardataGetconsscontainingvar(vardata); 
      int nbadconssVarisin = SCIPvardataGetnbadconssVarisin(vardata);

      for(int t=0;t<nbadconssVarisin;t++)
      {
         sum += dualmultipliers[varids[t]];
         // fprintf(varobj,"{%d, %f, %f\t",varids[t], dualmultipliers[varids[t]],sum);
      }

      
      // findmin += sum*solvals[v];
      // fprintf(varobj, "solval %f, coefficient %f, sum %f", solvals[v],sum, findmin);
      // fprintf(varobj,"}\n\n");
      SCIP_CALL(SCIPaddVarObj(scip,var,sum));
      // add = weights[v]+sum;
      
   }
      
   // SCIPinfoMessage(scip, TimeCollector, "\n finished changing the variable's weight after (sec) : %f\n", SCIPgetClockTime(scip, varslottime));
   
   SCIP_CALL(SCIPaddOrigObjoffset(scip,-1*sumofduals));
   // SCIP_CALL(SCIPprintOrigProblem(scip, problemstate, "lp", FALSE));
   SCIPsetMessagehdlrQuiet(scip, TRUE);
   // fclose(AfterPreProcessing);

   SCIP_CALL( SCIPtransformProb(scip) );
   SCIP_CALL( SCIPsolve(scip) );
   *relaxval = SCIPgetPrimalbound(scip);
   SCIPdebugMessage("relaxation bound = %e status = %d\n", *relaxval, SCIPgetStatus(scip));
   /*get the best solution*/   
   *bestsol = SCIPgetBestSol(scip) ;
   SCIP_CALL(SCIPallocBufferArray(scip,solvals,nvars+1)); 

   /*text output*/
   // fprintf(solutions,"first iteration \t bound=%f, \t objsol=%f \n",SCIPgetPrimalbound(scip),*relaxval);
   SCIP_CALL(SCIPprintBestSol(scip,solutions,FALSE));

   /*store the solution in solvals so we can later export it to subgradient function*/
   SCIPgetSolVals(scip,*bestsol,nvars,vars,*solvals);
   fclose(varobj);
   fclose(solutions);
   fclose(problemstate);

   return SCIP_OKAY;
}

SCIP_Real getnorm(SCIP_Real* array, int sizeofarray)
{
   SCIP_Real norm = 0;
   for(int r=0; r<sizeofarray;++r)
   {
      norm += array[r]*array[r];
   }
   norm=sqrt(norm);
   return norm;   
}