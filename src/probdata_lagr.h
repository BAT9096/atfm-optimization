/**@file   PROBDATA_lagr.h
 * @brief  Problem data for Lagrangian relaxation
 * @author Dawit Hailu
 * 
 *
 * This file handles the main problem data used in the Lagrangian relaxation.
 */
/*---+----1----+----2----+----3----+----4----+----d5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/
#ifndef __SCIP_PROBDATA_LAGR__
#define __SCIP_PROBDATA_LAGR__

#include "scip/scip.h"
#include "vardata_lagr.h"


/*classifying and storing the slot and start constraints in PROBDATA
create probdata*/
SCIP_RETCODE SCIPcreateprobdata
(   SCIP*                   relaxscip,
    SCIP_ProbData**         probdata,
    SCIP_VAR***             varbuffers,  
    int**                   badconss,        //will contain the ids of those rows that will be relaxed. 
    int**                   goodconss,         //will contain the number of variable in each row of the good constraints, i.e. those that won't be relaxed. 
    int*                    saveindexes
);

int SCIPgetallnconsvars(
SCIP_ProbData* probdata
    );
SCIP_RETCODE GetNGoodandNbad(
   SCIP* scip,
   int* nbad,
   int* ngood,
   SCIP_PROBDATA** probdata

);

/*will store the nconsvars in good constraints in an increasing order*/ 
int* SCIPprobdataGetStorenconsvars(
SCIP_ProbData* probdata
);

/*will store the arranged cons ids(row numbers) according to the nconsvars*/
int* SCIPprobdataGetArrangedgoodconss(
SCIP_ProbData* probdata
);


/*get's the ids of the variables found in the slot constraints*/
int* SCIPconsGetvarids(
    SCIP_ProbData* probdata
    );
int SCIPgetallmaxnconsvars(
SCIP_ProbData* probdata
);

int* SCIPlistnconsvars(
SCIP_ProbData* probdata
);

SCIP_Real getnorm(SCIP_Real* array, int sizeofarray);

int* SCIPlistconsvarids(
SCIP_ProbData* probdata
);

int* SCIPlistincreasing(
SCIP_ProbData* probdata
);

SCIP_CONS** SCIPgetSlotConss(
SCIP_ProbData* probdata
    );

int SCIPgetNSlotConss(
SCIP_ProbData* probdata
    );

/*in which good conss is a variable found in*/
int* SCIPVarinwhichgoodconss(
SCIP_ProbData* probdata
);

/*given an array of an initial solution array, this function will generate a feasible solution based on that array*/
SCIP_RETCODE SCIPgetFeasUpperbound(
   SCIP* scip,   
   SCIP_VARDATA** vardata,
   SCIP_VAR*** VARSinsidegoodconss,
   int** feasol,
   int nSlotConss,
   int* increasingorder,
   int* listnconsvars,
   int* listconsvarids,
   int* arrangedgoodconss,
   FILE** fromprob,
   SCIP_Real* weights,
   SCIP_Real* objVal,                               //final objective value in original problem, when multiplied with the original weight. 
   SCIP_SOL** mysol
   );

/* here we create the probdata, which will be called for storage of values to the data*/
SCIP_RETCODE probdataCreate(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_PROBDATA**       probdata,           /**< pointer to problem data */
   SCIP_CONS**          SlotConss,
//    SCIP_CONS**          StartConss,
   //SCIP_VAR**            vars,               /**< all exist variables */
   //SCIP_CONS**           conss,              /**< set partitioning constraints for each job exactly one */
   //int*                  varids,             /**< array of ids of variables in the slot constraints */
   //int                   nconss,              /**< number of constraints */
   int                   nSlotConss,          /**< number of slot constraints */
int                      nStartConss
   );

SCIP_RETCODE SCIPsetdualmultipliers(
   SCIP* scip,
   SCIP* relaxscip,
   SCIP_VARDATA* vardata,
   int* arrangedgoodconss,
   int* feasol,
   int nSlotConss,
   int stepsize,
   SCIP_SOL* relaxsolution,
   SCIP_Real** dualmultipliers,
   SCIP_Real* adjustmentoffice,
   SCIP_Real* weights,
   SCIP_Real* sumofduals,
   SCIP_VAR** VARSinsidegoodconss,
   FILE** dualfile
);

SCIP_RETCODE probdataFree(SCIP* scip, SCIP_PROBDATA** probdata);

SCIP_Real SCIPconsGetMultiplier(SCIP* scip,SCIP_CONS** cons,SCIP_Real subgradient,SCIP_Real C, SCIP_Real stepsize,SCIP_Bool firstiteration, SCIP_Real dualval);

/******************************************************************************************************************/
/* the next step would be to maximize over the Lagrangian dual Z(dual). This would give us the first iteration     */
//* subgradient^{0}_{r} = sum{x[v]}-1, where x[v] is the solution value of the variables founds in the r-th conss  */
/* the abover formula gives us the 0'th iteration of the subgradiant vector                                        */
/*******************************************************************************************************************/
int SCIPgetSubgradients(SCIP* scip, int r, int* feasol, int* listconsvarids, int* listnconsvars, int* increasingorder);

SCIP_RETCODE SCIPclassifyGoodBad(SCIP* scip, SCIP_CONS** conss, int nconss, SCIP_ProbData** probdata);

SCIP_RETCODE SCIPsolveiteration(SCIP* scip,int nSlotConss,SCIP_Real** subgradients, SCIP_Real C, SCIP_Real* stepsize,SCIP_Real** bestsolvals,SCIP_Real* upperbound,int niter);

#endif
